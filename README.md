# EC2 Information Crawler
## Overview
This project is a small Django project to display the various types of EC2 instance types available on Amazon. The
information is obtained from [ec2instances.info](https://www.ec2instances.info). The crawler is built using 
[Scrapy](https://scrapy.org/). Database used is SQLite3.

## Requirements
* Python (3.6.5)
* Django (2.0+)
* Scrapy (1.5.1)

## Quickstart
### Installation
* Setup python virtual environment (I use pyenv, virtualenv works too) and install requirements.
    ```bash
    $ pyenv activate venv
    $ (venv) pip install -r requirements.txt
    ```
* Setup the SQLite3 DB file
    ```bash
    $ (venv) python manage.py migrate
    ```
* Populate EC2 information by running the following command
    ```bash
    $ (venv) python manage.py runscript refresh_db
    ```

### Deployment
```bash
$ (venv) python manage.py runserver
```

### API
* `/` - Web application
* `/ec2/` - EC2 Endpoint


