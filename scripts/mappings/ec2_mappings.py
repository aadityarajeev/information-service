ON_DEMAND_COST_MAPPINGS = {
    'linux': 'cost-ondemand cost-ondemand-linux',
    'rhel': 'cost-ondemand cost-ondemand-rhel',
    'sles': 'cost-ondemand cost-ondemand-sles',
    'mswin': 'cost-ondemand cost-ondemand-mswin',
    'mswin_sql_web': 'cost-ondemand cost-ondemand-mswinSQLWeb',
    'mswin_sql': 'cost-ondemand cost-ondemand-mswinSQL',
    'mswin_sql_enterprise': 'cost-ondemand cost-ondemand-mswinSQLEnterprise',
    'linux_sql_web': 'cost-ondemand cost-ondemand-linuxSQLWeb',
    'linux_sql': 'cost-ondemand cost-ondemand-linuxSQL',
    'linux_sql_enterprise': 'cost-ondemand cost-ondemand-linuxSQLEnterprise',
    'cost_emr': 'cost-emr cost-ondemand'
}

RESERVED_COST_MAPPINGS = {
    'linux': 'cost-reserved cost-reserved-linux',
    'rhel': 'cost-reserved cost-reserved-rhel',
    'sles': 'cost-reserved cost-reserved-sles',
    'mswin': 'cost-reserved cost-reserved-mswin',
    'mswin_sql_web': 'cost-reserved cost-reserved-mswinSQLWeb',
    'mswin_sql': 'cost-reserved cost-reserved-mswinSQL',
    'mswin_sql_enterprise': 'cost-reserved cost-reserved-mswinSQLEnterprise',
    'linux_sql_web': 'cost-reserved cost-reserved-linuxSQLWeb',
    'linux_sql': 'cost-reserved cost-reserved-linuxSQL',
    'linux_sql_enterprise': 'cost-reserved cost-reserved-linuxSQLEnterprise'
}
