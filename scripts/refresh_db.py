import scrapy
import re

from scripts.mappings.ec2_mappings import ON_DEMAND_COST_MAPPINGS, RESERVED_COST_MAPPINGS
from ec2.models import EC2
from ec2.serializers import EC2Serializer
from scrapy.crawler import CrawlerProcess


class EC2Spider(scrapy.Spider):
    name = 'ec2-spider'
    start_urls = ["https://ec2instances.info/"]
    row_classes = []
    on_demand_cost_classes = []
    reserved_cost_classes = []

    def parse(self, response):
        EC2.objects.all().delete()
        self.logger.info("Purged EC2 Information")
        rows = iter(response.xpath('//*[@id="data"]//tbody').xpath('//tr'))
        next(rows)
        for row in rows:
            if not self.row_classes:
                self.row_classes, self.on_demand_cost_classes, self.reserved_cost_classes = self.get_classes(row)
            row_data = self.get_data(row)
            self.format_data(row_data)
            row_data = self.organize(row_data)
            EC2Serializer.create(EC2Serializer(), row_data)
            self.logger.info("Added: "+row_data.get('apiname', 'unavailable'))
            del row_data

    def get_data(self, row):
        data = {}
        for key in self.row_classes:
            no_span = row.xpath('td[@class="' + key + '"]/text()[normalize-space()]').extract()
            span = row.xpath('td[@class="' + key + '"]//span/text()[normalize-space()]').extract()
            if no_span:
                data[key] = EC2Spider.remove_extra_whitespaces(no_span[0])
            elif span:
                data[key] = EC2Spider.remove_extra_whitespaces(span[0])
            else:
                data[key] = None
        return data

    @staticmethod
    def format_data(row):
        cost_regex = re.compile(r'^\$(\d+\.?\d*) hourly$')
        fp_metrics_regex = re.compile(r'^(\d+\.?\d*) (Mbps|MB/s|IOPS)$')
        for (k, v) in row.items():
            # Handle None's
            if not v:
                continue
            if v.lower() == 'n/a' or v.lower() == 'unknown' or v.lower() == 'unavailable':
                row[k] = None
            # Handle Integers
            elif v.isdigit():
                row[k] = int(v)
            # Handle Booleans
            elif v.lower() == 'yes' or v.lower() == 'no':
                row[k] = v.lower() == 'yes'
            # Handle FP Metrics
            elif fp_metrics_regex.match(v):
                row[k] = float(fp_metrics_regex.match(v).group(1))
            # Handle costs
            elif cost_regex.match(v):
                row[k] = float(cost_regex.match(v).group(1))

    def organize(self, row):
        organized_data = {k.replace('-', '_'): v for (k, v) in row.items() if k not in self.on_demand_cost_classes
                          and k not in self.reserved_cost_classes}
        organized_data["on_demand_costs"] = {k: row.get(v) for (k, v) in ON_DEMAND_COST_MAPPINGS.items()}
        organized_data["reserved_costs"] = {k: row.get(v) for (k, v) in RESERVED_COST_MAPPINGS.items()}
        return organized_data

    @staticmethod
    def remove_extra_whitespaces(string: str):
        return ' '.join(string.split())

    @staticmethod
    def get_classes(row):
        classes = row.xpath('td/@class')
        row_classes = list(map(lambda x: x.extract(), classes))
        on_demand_cost_classes = [v for v in row_classes if 'cost-ondemand' in v]
        reserved_cost_classes = [v for v in row_classes if 'cost-reserved' in v]
        return row_classes, on_demand_cost_classes, reserved_cost_classes


def run():
    process = CrawlerProcess({
        'USER-AGENT': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5'
    })
    process.crawl(EC2Spider)
    process.start()
