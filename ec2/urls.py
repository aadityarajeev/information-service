from django.urls import path, re_path
from .views import ListCreateEC2, RetrieveUpdateDestroyEC2

urlpatterns = [
    path('', ListCreateEC2.as_view(), name='list-create-ec2'),
    re_path(r'^(?P<pk>[\w.]+)/$', RetrieveUpdateDestroyEC2.as_view(), name='retrieve-update-destroy-ec2')
]
