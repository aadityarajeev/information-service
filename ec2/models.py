from django.db import models


class OnDemandCost(models.Model):
    linux = models.FloatField(null=True)
    rhel = models.FloatField(null=True)
    sles = models.FloatField(null=True)
    mswin = models.FloatField(null=True)
    mswin_sql_web = models.FloatField(null=True)
    mswin_sql = models.FloatField(null=True)
    mswin_sql_enterprise = models.FloatField(null=True)
    linux_sql_web = models.FloatField(null=True)
    linux_sql = models.FloatField(null=True)
    linux_sql_enterprise = models.FloatField(null=True)
    cost_emr = models.FloatField(null=True)

    class Meta:
        db_table = "OnDemandCost"


class ReservedCost(models.Model):
    linux = models.FloatField(null=True)
    rhel = models.FloatField(null=True)
    sles = models.FloatField(null=True)
    mswin = models.FloatField(null=True)
    mswin_sql_web = models.FloatField(null=True)
    mswin_sql = models.FloatField(null=True)
    mswin_sql_enterprise = models.FloatField(null=True)
    linux_sql_web = models.FloatField(null=True)
    linux_sql = models.FloatField(null=True)
    linux_sql_enterprise = models.FloatField(null=True)

    class Meta:
        db_table = "ReservedCost"


class EC2(models.Model):
    name = models.CharField(max_length=100, null=False)
    apiname = models.CharField(max_length=25, null=False, primary_key=True)
    memory = models.CharField(max_length=25, null=True)
    computeunits = models.CharField(max_length=25, null=True)
    vcpus = models.CharField(max_length=50, null=True)
    gpus = models.IntegerField(null=True)
    fpga = models.IntegerField(null=True)
    ecu_per_vcpu = models.CharField(max_length=25, null=True)
    physical_processor = models.CharField(max_length=50, null=True)
    clock_speed_ghz = models.CharField(max_length=25, null=True)
    intel_avx = models.NullBooleanField(default=None)
    intel_avx2 = models.NullBooleanField(default=None)
    intel_turbo = models.NullBooleanField(default=None)
    storage = models.CharField(max_length=50, null=True)
    warmed_up = models.NullBooleanField(default=None)
    trim_support = models.NullBooleanField(default=None)
    architecture = models.CharField(max_length=25, null=True)
    networkperf = models.CharField(max_length=50, null=True)
    ebs_max_bandwidth = models.FloatField(null=True)
    ebs_throughput = models.FloatField(null=True)
    ebs_iops = models.FloatField(null=True)
    ebs_as_nvme = models.NullBooleanField(default=None)
    maxips = models.IntegerField(null=True)
    maxenis = models.IntegerField(null=True)
    enhanced_networking = models.NullBooleanField(default=None)
    vpc_only = models.NullBooleanField(default=None)
    ipv6_support = models.NullBooleanField(default=None)
    placement_group_support = models.NullBooleanField(default=None)
    linux_virtualization = models.CharField(max_length=25, null=True)
    emr_support = models.NullBooleanField(default=None)
    cost_ebs_optimized = models.FloatField(null=True)
    on_demand_costs = models.OneToOneField(OnDemandCost, on_delete=models.CASCADE)
    reserved_costs = models.OneToOneField(ReservedCost, on_delete=models.CASCADE)

    class Meta:
        db_table = "EC2"
