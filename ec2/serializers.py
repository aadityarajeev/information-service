from rest_framework import serializers
from .models import *


class OnDemandCostSerializer(serializers.ModelSerializer):
    class Meta:
        model = OnDemandCost
        exclude = ['id']


class ReservedCostSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReservedCost
        exclude = ['id']


class EC2Serializer(serializers.ModelSerializer):
    on_demand_costs = OnDemandCostSerializer(required=False)
    reserved_costs = ReservedCostSerializer(required=False)

    class Meta:
        model = EC2
        fields = '__all__'

    def create(self, validated_data):
        on_demand_costs_data = validated_data.pop('on_demand_costs')
        reserved_costs_data = validated_data.pop('reserved_costs')
        on_demand_costs = OnDemandCostSerializer.create(OnDemandCostSerializer(), validated_data=on_demand_costs_data)
        reserved_costs = ReservedCostSerializer.create(ReservedCostSerializer(), validated_data=reserved_costs_data)
        ec2 = EC2.objects.update_or_create(on_demand_costs=on_demand_costs, reserved_costs= reserved_costs,
                                           **validated_data)
        return ec2

    def update(self, instance: EC2, validated_data):
        on_demand_costs_data = validated_data.pop('on_demand_costs')
        reserved_costs_data = validated_data.pop('reserved_costs')

        on_demand_costs = instance.on_demand_costs
        reserved_costs = instance.reserved_costs

        # Handle On-Demand Costs
        on_demand_costs.linux = on_demand_costs_data.get('linux', on_demand_costs.linux)
        on_demand_costs.rhel = on_demand_costs_data.get('rhel', on_demand_costs.rhel)
        on_demand_costs.sles = on_demand_costs_data.get('sles', on_demand_costs.sles)
        on_demand_costs.mswin = on_demand_costs_data.get('mswin', on_demand_costs.mswin)
        on_demand_costs.mswin_sql_web = on_demand_costs_data.get('mswin_sql_web', on_demand_costs.mswin_sql_web)
        on_demand_costs.mswin_sql = on_demand_costs_data.get('mswin_sql', on_demand_costs.mswin_sql)
        on_demand_costs.mswin_sql_enterprise = on_demand_costs_data.get('mswin_sql_enterprise',
                                                                        on_demand_costs.mswin_sql_enterprise)
        on_demand_costs.linux_sql_web = on_demand_costs_data.get('linux_sql_web', on_demand_costs.linux_sql_web)
        on_demand_costs.linux_sql = on_demand_costs_data.get('linux_sql', on_demand_costs.linux_sql)
        on_demand_costs.linux_sql_enterprise = on_demand_costs_data.get('linux_sql_enterprise',
                                                                        on_demand_costs.linux_sql_enterprise)
        on_demand_costs.cost_emr = on_demand_costs_data.get('cost_emr', on_demand_costs.cost_emr)
        on_demand_costs.save()

        # Handle Reserved Costs
        reserved_costs.linux = reserved_costs_data.get('linux', reserved_costs.linux)
        reserved_costs.rhel = reserved_costs_data.get('rhel', reserved_costs.rhel)
        reserved_costs.sles = reserved_costs_data.get('sles', reserved_costs.sles)
        reserved_costs.mswin = reserved_costs_data.get('mswin', reserved_costs.mswin)
        reserved_costs.mswin_sql_web = reserved_costs_data.get('mswin_sql_web', reserved_costs.mswin_sql_web)
        reserved_costs.mswin_sql = reserved_costs_data.get('mswin_sql', reserved_costs.mswin_sql)
        reserved_costs.mswin_sql_enterprise = reserved_costs_data.get('mswin_sql_enterprise',
                                                                        reserved_costs.mswin_sql_enterprise)
        reserved_costs.linux_sql_web = reserved_costs_data.get('linux_sql_web', reserved_costs.linux_sql_web)
        reserved_costs.linux_sql = reserved_costs_data.get('linux_sql', reserved_costs.linux_sql)
        reserved_costs.linux_sql_enterprise = reserved_costs_data.get('linux_sql_enterprise',
                                                                        reserved_costs.linux_sql_enterprise)
        reserved_costs.save()

        instance.name = validated_data.get('name', instance.name)
        instance.apiname = validated_data.get('apiname', instance.apiname)
        instance.memory = validated_data.get('memory', instance.memory)
        instance.computeunits = validated_data.get('computeunits', instance.computeunits)
        instance.vcpus = validated_data.get('vcpus', instance.vcpus)
        instance.gpus = validated_data.get('gpus', instance.gpus)
        instance.fpga = validated_data.get('fpga', instance.fpga)
        instance.ecu_per_vcpu = validated_data.get('ecu_per_vcpu', instance.ecu_per_vcpu)
        instance.physical_processor = validated_data.get('physical_processor', instance.physical_processor)
        instance.clock_speed_ghz = validated_data.get('clock_speed_ghz', instance.clock_speed_ghz)
        instance.intel_avx = validated_data.get('intel_avx', instance.intel_avx)
        instance.intel_avx2 = validated_data.get('intel_avx2', instance.intel_avx2)
        instance.intel_turbo = validated_data.get('intel_turbo', instance.intel_turbo)
        instance.storage = validated_data.get('storage', instance.storage)
        instance.warmed_up = validated_data.get('warmed_up', instance.warmed_up)
        instance.trim_support = validated_data.get('trim_support', instance.trim_support)
        instance.architecture = validated_data.get('architecture', instance.architecture)
        instance.networkperf = validated_data.get('networkperf', instance.networkperf)
        instance.ebs_max_bandwidth = validated_data.get('ebs_max_bandwidth', instance.ebs_max_bandwidth)
        instance.ebs_throughput = validated_data.get('ebs_throughput', instance.ebs_throughput)
        instance.ebs_iops = validated_data.get('ebs_iops', instance.ebs_iops)
        instance.ebs_as_nvme = validated_data.get('ebs_as_nvme', instance.ebs_as_nvme)
        instance.maxips = validated_data.get('maxips', instance.maxips)
        instance.maxenis = validated_data.get('maxenis', instance.maxenis)
        instance.enhanced_networking = validated_data.get('enhanced_networking', instance.enhanced_networking)
        instance.vpc_only = validated_data.get('vpc_only', instance.vpc_only)
        instance.ipv6_support = validated_data.get('ipv6_support', instance.ipv6_support)
        instance.placement_group_support = validated_data.get('placement_group_support',
                                                              instance.placement_group_support)
        instance.linux_virtualization = validated_data.get('linux_virtualization', instance.linux_virtualization)
        instance.emr_support = validated_data.get('emr_support', instance.emr_support)
        instance.cost_ebs_optimized = validated_data.get('cost_ebs_optimized', instance.cost_ebs_optimized)
        instance.on_demand_costs = on_demand_costs
        instance.reserved_costs = reserved_costs
        instance.save()
        return instance
