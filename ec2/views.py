from rest_framework import generics
from .models import EC2
from .serializers import EC2Serializer


class ListCreateEC2(generics.ListCreateAPIView):
    """
    Lists and Create EC2 information
    """
    queryset = EC2.objects.all()
    serializer_class = EC2Serializer


class RetrieveUpdateDestroyEC2(generics.RetrieveUpdateDestroyAPIView):
    """
    Lists, update and deletes single EC2 instance type information
    """
    queryset = EC2.objects.all()
    serializer_class = EC2Serializer

